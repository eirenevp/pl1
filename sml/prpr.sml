(*1.01, letztes Element*)
fun my_last nil     = nil  
  | my_last [h]     = [h]
  | my_last (h::t)  = my_last t

(*1.02, zweiletztes Element*)  
fun my_zweilast nil     = nil  
  | my_zweilast [h]     = [h]
  | my_zweilast (h::t)  = my_last t


