fun mul x 0 = 0
  | mul x y =
    let
      val en_mul = 2*(mul x (y div 2))
    in
     if (y mod 2 = 0) then en_mul
     else (x + en_mul) 
    end

