 /*1.01*/
my_last(L,[L]).
my_last(L,[_|Ls]) :- my_last(L,Ls).

/*1.02*/
my_zweiLetztes(PL,[PL,_]).
my_zweiLetztes(Ls,[_|L]) :- my_zweiLetztes(Ls,L).

/*1.02+*/
my_element(X,[X|_]).
my_element(X,[_|L]) :- my_element(X,L).

/*1.03*/
element_at(X,[X|_],1).
element_at(X,[_|Ls],K) :- K > 0, K1 is K-1, element_at(X,Ls,K1).

/*1.04*/
my_length([],0).
my_length([_|Ls],N) :- my_length(Ls,N1), N is N1 + 1.

/*1.05*/
my_reverse(L1,L2) :- my_rev(L1,L2,[]).

my_rev([],L2,L2) :- !.
my_rev([X|Xs],L2,Acc) :- my_rev(Xs,L2,[X|Acc]).

/*1.06*/
is_palindrome(L) :- reverse(L,L).

/*1.07*/
my_flatten(X,[X]) :- \+ is_list(X).
my_flatten([],[]).
my_flatten([X|Xs],Zs) :- my_flatten(X,Y), my_flatten(Xs,Ys), append(Y,Ys,Zs).

/*1.08*/
compress([],[]).
compress([X],[X]).
compress([X,X|Xs],Zs) :- compress([X|Xs],Zs).
compress([X,Y|Ys],[X|Zs]) :- X \= Y, compress([Y|Ys],Zs).

/*1.09*/
pack([],[]).
pack([X|Xs],[Z|Zs]) :- transfer(X,Xs,Ys,Z), pack(Ys,Zs).

transfer(X,[],[],[X]).
transfer(X,[Y|Ys],[Y|Ys],[X]) :- X \= Y.
transfer(X,[X|Xs],Ys,[X|Zs]) :- transfer(X,Xs,Ys,Zs).

/*1.10*/
encode(L1,L2) :- pack(L1,L), transform(L,L2).

transform([],[]).
transform([[X|Xs]|Ys],[[N,X]|Zs]):- length([X|Xs],N), transform(Ys,Zs).

/*1.11*/
encode_modified(L1,L2) :- encode(L1,L), strip(L,L2).

strip([],[]).
strip([[1,X]|Ys],[X,Zs]) :- strip(Ys,Zs).
strip([[N,X]|Ys],[[N,X]|Zs]) :- N > 1, strip(Ys,Zs).

/*1.12*/
decode([],[]).
decode([X|Ys],[Y|Zs]) :- \+ is_list(X), decode(Ys,Zs).
decode([[1,X]|Ys],[Ys|Zs]) :- decode(Ys,Zs).
decode([[N,X]|Ys],[X|Zs]) :- N > 1, N1 is N-1,  decode([[N1,X]|Ys],Zs).

/*1.14*/
dupli([],[]).
dupli([X|Xs],[X,X|Ys]) :- dupli(Xs,Ys).

/*1.15*/
dupli(L1,N,L2) :- dupli(L1,N,L2,N).

dupli([],_,[],_).
dupli([_|Xs],N,Ys,0) :- dupli(Xs,N,Ys,N).
dupli([X|Xs],N,[X|Ys],K) :- K > 0, K1 is K-1, dupli([X|Xs],N,Ys,K1).

/*1.16*/
drop(L1,N,L2) :- drop(L1,N,L2,N).

drop([],_,[],_).
drop([_|Xs],N,Xs,1) :- drop(Xs,N,Xs,N).
drop([X|Xs],N,[X|Xs],K) :- K > 0,K1 is K-1, drop(Xs,N,Xs,K1).

/*1.17*/
split(L,0,[],L).
split([X|Xs],N,[X|Ys],Zs) :- N > 0, N1 is N-1, split(Xs,N1,Ys,Zs).


/*1.18*/
slice([X|_],1,1,[X]).
slice([X|Xs],1,K,[X|Ys]) :- K > 0, K1 is K-1, slice(Xs,1,K1,Ys).
slice([_|Xs],I,K,Ys) :- I > 0, I1 is I-1, K1 is K-1, slice(Xs,I1,K1,Ys).

/*1.19*/
%rotate([],_,[]).
%rotate(L,N,Y) :- split(L,N,L1,L2), append(L2,L1,Y).
%rotate(L,N,Y) :- N < 0, length(L,Len), split(L,(Len+N),L1,L2), append(L2,L1,Y).

rotate(L1,N,L2) :- N >= 0, length(L1,NL1), N1 is N mod NL1, rotate_left(L1,N1,L2).
rotate(L1,N,L2) :- N < 0, length(L1,NL1), N1 is NL1 + (N mod NL1), rotate_left(L1,N1,L2).

rotate_left(L,0,L).
rotate_left(L1,N,L2) :- N > 0, split(L1,N,S1,S2),append(S2,S1,L2).

/*1.20*/
remove_at(X,[X|Xs],1,Xs).
remove_at(X,[Y|Xs],K,[Y|Ys]) :- K > 0, K1 is K-1, remove_at(X,Xs,K1,Ys).

/*1.21*/
insert_at(X,L,K,R) :- remove_at(X,R,K,L).

/*1.22*/
range(X,X,[X]).
range(Ka,Pa,[Ka|Xs]) :- Ka < Pa, Ka1 is Ka+1, range(Ka1,Pa,Xs).

/********/

/*1.26*/
combination(0,_,[]).
combination(K,L,[X|Xs]) :- K > 0, el(X,L,R), K1 is K-1, combination(K1,R,Xs).

el(X,[X|L],L).
el(X,[_|L],R) :- el(X,L,R).

parent(katie,iren).
parent(katie,lydia).

siblings(X,Y) :- 
    parent(P,X),
    parent(P,Y),
    \+(X = Y).

subseq([],[]).
subseq([Item|RestX],[Item|RestY]) :- subseq(RestX,RestY).
subseq(X,[_|RestY]) :- subseq(X,RestY).
